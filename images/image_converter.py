from PIL import Image

name = 'sec'
filename = f'images/{name}.png'
img = Image.open(filename)
img.save(f'images/{name}.ico', format='ICO', sizes=[(512,512)])


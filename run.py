from platform import java_ver
import tkinter as tk
from ctypes import windll

from tkinter import ttk, filedialog
from tkinter.messagebox import showinfo

import subprocess
from pathlib import Path

windll.shcore.SetProcessDpiAwareness(1)
root = tk.Tk()

# title
root.title('TextSecretary')
root.geometry('300x100+50+50')
# label window
message = tk.Label(root, text="Monthly Helper!")
message.pack()

# choose csv
def select_file():
    filetypes = (
        ('csv', '*.csv'),
    )
    filename = filedialog.askopenfilename(
        title = 'Select Monthly csv',
        initialdir=Path.home()/'Downloads',
        filetypes=filetypes
    )
    output = Path(filename).parents[0] / "i_love_my_husband.txt"
    subprocess.run(['python', './scripts/run.py', f'--csv={filename}', f'--output={output}'   ])
    subprocess.run(['notepad', output])
    root.destroy()
    
# open button
open_button = ttk.Button(
    root,
    text='Start',
    command=select_file
)
open_button.pack(expand=True)

# keep window open
root.mainloop()
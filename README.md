# kayla-automation

This repo was made for you, Kayla! Just clone this repo, and put the shortcut in `/dist/` anywhere you want and double click it to get started!

## Making a shortcut

- right click `run.ps1` and create a shortcut
- right click the shortcut and edit the following properties:
    - change icon -> find a `.ico` file you like
    - ^ you can convert `.png` to `.ico` with `images/image_converter.py`
    - Change the `target` field to:
```
C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -command "& 'C:\Users\kayla\Documents\Rob\kayla-automation\run.ps1'"
```
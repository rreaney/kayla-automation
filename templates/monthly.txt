Good {{Time of Day}}, {{Student First Name}} & Family! 
Here is our current progress report for English 2:

Overall Grade: {{Course Work Current Grade}}
Last Assignment Submitted: {{Last Assignment Turned In Date}}
Percent Complete with Current Semester: {{Percent Complete}}
Weeks Behind: {{Number Weeks Behind}}
Estimated Completion Date Based on Attendance: {{Estimated Completion Date}}

Let me know if that completion date works for you. Keep me posted with how I can help this week 😊 -Ms. Kelly
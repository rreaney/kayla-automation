from pathlib import Path
from typing import Union
import pandas as pd
from datetime import datetime

class Parser:
    def __init__(self, path: Union[Path, str], output: Union[Path, str] = None):
        """A parser for TextSecretary
        args:
            path (Path, str): Path to data csv
            output (Path, str, optional): Path to output .txt to save results
        """
        if output is None:
            output=str(datetime.now()) + '.txt'
        self.output = output
        self.path = Path(path)
        self.students = pd.read_csv(path).to_dict('records')
        self.lines = None

    def read_template(self, template):
        with open(template, 'r') as f:
            self.lines = f.readlines()

    def replace_templates(self, student, lines):
        # do templating
        final_lines = []
        for line in lines:
            for var in student: # for each key in the student dict replace that value
                line = line.replace(f'{{{{{var}}}}}', str(student[var]))
            final_lines.append(line)
        return final_lines

        
from datetime import datetime
from pathlib import Path

from parent import Parser

class Monthly(Parser):
    template = Path('templates/monthly.txt')

    def format_record(self, record):
        name = record['Student Name']
        last_assign_date = record['Last Assignment Turned In Date']
        completion_date = record['Estimated Completion Date']
        percent_complete = record['Percent Complete']
        current_grade = record['Course Work Current Grade']
        weeks_behind = record['Number Weeks Behind']

        def behind_format(x):
            if x < 0:
                return 'N/A - on track'
            else:
                return str(x)

        def get_grade(x):
            # grade = ground(x, 2)
            grade = x
            if x >= 90:
                return f'{grade}% A'
            elif x>= 80:
                return f'{grade}% B'
            elif x>= 70:
                return f'{grade}% C'
            elif x>= 60:
                return f'{grade}% D'
            else:
                return f'{grade}% F'

        time = datetime.now().hour
        if time < 12:
            greet = 'morning'
        else:
            greet = 'afternoon'

        temp = {
            'Student First Name': name.split(',')[1].strip().title(),
            'Last Assignment Turned In Date': str((datetime.now() - datetime.strptime(last_assign_date, '%m/%d/%Y')).days) + " days ago", # number of days since last assign
            'Estimated Completion Date': completion_date,#.split()[0].lower() + ' ' + ' '.join(completion_date.split()[1:]),
            'Percent Complete': str(percent_complete) +'%',
            'Course Work Current Grade': get_grade(current_grade),
            'Number Weeks Behind': behind_format(weeks_behind),
            'Time of Day': greet
        }
        return temp

    def run(self):
        self.read_template(Monthly.template)
        with open(self.output, 'w') as f:
            for student in map(self.format_record, self.students):
                newfile = self.replace_templates(student, self.lines)
                for line in newfile:
                    f.write(line)
                f.write('\n \n#######################\n#######################\n#######################\n \n')
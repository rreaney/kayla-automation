from datetime import datetime
import pandas as pd
from argparse import ArgumentParser
import sys
from pathlib import Path

from methods import Monthly

if __name__ == '__main__':
    path = Path(sys.argv[0]).parents[0]
    
    parser = ArgumentParser(description='Generate nonworker text.')
    parser.add_argument('--csv', required=True, help='path to data csv')
    parser.add_argument('--output', required=False, help='path to output file')
    #parser.add_argument('--method', help='text generation method')
    args = parser.parse_args()

    print(f"args.output {args.output} has type {type(args.output)}")
    helper = Monthly(args.csv, args.output)
    helper.run()
    print("Thank you for using TextSecretary.")

